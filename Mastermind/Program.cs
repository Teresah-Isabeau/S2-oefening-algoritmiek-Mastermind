﻿using System;

namespace Mastermind
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let's play MasterMind!");
            Console.WriteLine("----------------------");

            int numberLength = 5; //geeft de lengte voor het aantal nummers

            Console.WriteLine("The number has 5 digits with numbers 0 and 1, you have 10 attempts to guess the right anwser!");
            Console.WriteLine("");
            int[] arrayOfNumbers = RandomNumbers(numberLength); //geeft randomnummer voor numberlength(in dit geval 5)
            Console.WriteLine("Good Luck!"); 
            Console.WriteLine("___________________________________________________________________________");
            Console.WriteLine("");            
            bool won = false; //default voor win is false

            //User heeft 10 pogingen om te raden
            string input = "";
            for (int allowedAttempts = 10; allowedAttempts > 0; allowedAttempts--)
            {
                Console.WriteLine("Enter your guess");
                input = Console.ReadLine();
                won = UserGuess(input, arrayOfNumbers); //checkt of input gelijk is aan random arrayOfNumbers
                Hits(arrayOfNumbers, input); //checkt hoeveel hits en misses er zijn
                if (won)
                {
                    break;
                }
            }

            if(won == false) //als het systeem klaar is met de for loop en de gebruiker het niet juist heeft geraden, verliest het spel en krijgt deze melding
            {
                Console.WriteLine("");
                Console.WriteLine("Oh no, you couldn't guess the right number...");
                Console.WriteLine("The correct number was: " + ConvertIntArrayToString(arrayOfNumbers));     
            }

            else //als de gebruiker het juist heeft geraden win hij en krijgt deze melding
            {
                Console.WriteLine("");
                Console.WriteLine(input + " was correct! You win!");
            }
        }

        /// <summary>
        /// convert random array of number naar een string
        /// </summary>
        /// <param name="userinput"></param>
        /// <param name="answer"></param>
        public static string ConvertIntArrayToString(int[] answer)
        {
            string userAnswer = "";
            for (int i = 0; i < answer.Length; i++)
            {
                userAnswer += Convert.ToString(answer[i]);
            }
            return userAnswer;
        }


        /// <summary>
        /// Checkt of input en arrayOfNumbers hetzelfde zijn
        /// </summary>
        /// <param name="userinput"></param>
        /// <param name="answer"></param>
        public static bool UserGuess(string userinput, int[] answer)
        {
            return ConvertIntArrayToString(answer) == userinput;
        }

        /// <summary>
        /// RandomNumbers genereerd random nummers van 0 en 1 in een for loop.
        /// returned random nummers
        /// </summary>
        /// <param name="Size"></param>
        /// <returns></returns>
        public static int[] RandomNumbers(int Size)
        {
            int eachNumber;
            int[] randomNumber = new int[Size];
            Random random = new Random();

            for (int i = 0; i < Size; i++)
            {
                eachNumber = random.Next(0, 2);
                randomNumber[i] = eachNumber;
            }
            return randomNumber;
        }

        /// <summary>
        /// Hits kijkt of er getallen van de gebruiker en het systeem op dezelfde plek staan
        /// </summary>
        /// <param name="randomArray"></param>
        /// <param name="userArray"></param>
        /// <returns></returns>
        public static int Hits(int[] randomArray, string userArray)
        {
            int hits = 0;

            for (int i = 0; i < randomArray.Length; i++)
            {
                if (randomArray[i] == int.Parse(userArray[i].ToString()))
                {
                    hits++;
                }
            }

            Console.WriteLine("Results: {0} Hit(s), {1} Miss(es)", hits, randomArray.Length - hits);
            Console.WriteLine("");
            return hits;
        }
    }
}
